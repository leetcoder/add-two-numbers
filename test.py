#!/usr/bin/python3
import unittest

from ListNode import ListNode, Solution

class TestListNode(unittest.TestCase):

    def test_constructor(self):
        l = ListNode(1)
        self.assertEqual(l.val, 1)
        self.assertEqual(l.next, None)
    
    def test_fromList(self):
        l = ListNode.fromList([2,3])
        self.assertEqual(l.val, 2)
        self.assertEqual(l.next.val, 3)
        self.assertEqual(l.next.next, None)

class TestSolution(unittest.TestCase):

    def test_addTwoNumbers_with_constructor(self):
        l1 = ListNode(3)
        l2 = ListNode(9)
        result = Solution().addTwoNumbers(l1, l2)
        self.assertEqual(result.val, 2)
        self.assertEqual(result.next.val, 1)
        self.assertEqual(result.next.next, None)

    def test_addTwoNumbers_with_setStringValue(self):
        l1 = ListNode(3)
        l2 = ListNode(9)
        result = Solution().addTwoNumbers(l1, l2)
        result.setStringValue()
        print(result.stringValue)
        self.assertEqual(result.stringValue, "2 -> 1")

    def test_addTwoNumbers_with_fromList(self):
        l1 = ListNode.fromList([2,4,3])
        l2 = ListNode.fromList([5,6,4])
        result = Solution().addTwoNumbers(l1, l2)
        result.setStringValue()
        print(result.stringValue)
        self.assertEqual(result.stringValue, "7 -> 0 -> 8")

if __name__ == "__main__":
    unittest.main()
