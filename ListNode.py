#!/usr/bin/python3

# Definition for singly-linked list.
class ListNode:

    def __init__(self, x: int):
        self.val = x
        self.next = None
        self.stringValue = ""

    @classmethod
    def fromList(cls, l: list):
        if len(l) > 0:
            first = l.pop(0)
            result = cls(first)
            result.next = cls.fromList(l)
            return result

    def setStringValue(self, firstItem = None):
        if firstItem == None:
            firstItem = self
        firstItem.stringValue += str(self.val)
        if self.next:
            firstItem.stringValue += " -> "
            self.next.setStringValue(firstItem)

class Solution:
    def addTwoNumbers(self, l1: ListNode, l2: ListNode, carry = 0) -> ListNode:
        total = l1.val + l2.val + carry
        carry = total // 10
        result = ListNode(total % 10)
        if l1.next or l2.next or carry !=0:
            if l1.next == None:
                l1.next = ListNode(0)
            if l2.next == None:
                l2.next = ListNode(0)
            result.next = self.addTwoNumbers(l1.next, l2.next, carry)
        return result

if __name__ == "__main__":
    l = ListNode(1)
    print(l.val)

    l = ListNode.fromList([2,3])
    print(l.val)
    print(l.next.val)

    result = Solution().addTwoNumbers(ListNode(3), ListNode(9))
    result.setStringValue()
    print(result.stringValue)

    l1 = ListNode.fromList([2,4,3])
    l2 = ListNode.fromList([5,6,4])
    result = Solution().addTwoNumbers(l1, l2)
    result.setStringValue()
    print(result.stringValue)
